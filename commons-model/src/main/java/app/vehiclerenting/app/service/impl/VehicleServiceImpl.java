package app.vehiclerenting.app.service.impl;

import app.vehiclerenting.app.model.EntityStatus;
import app.vehiclerenting.app.model.User;
import app.vehiclerenting.app.model.Vehicle;
import app.vehiclerenting.app.model.VehicleType;
import app.vehiclerenting.app.repository.VehicleRepository;
import app.vehiclerenting.app.service.VehicleService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class VehicleServiceImpl implements VehicleService {

    private VehicleRepository vehicleRepository;

    @Override
    public Vehicle create(Vehicle vehicle) {
        return edit(vehicle);
    }

    @Override
    public Page<Vehicle> list(Pageable pageable) {
        return vehicleRepository.findVehiclesByStatus(EntityStatus.ACTIVE, pageable);
    }

    @Override
    public Vehicle edit(Vehicle vehicle) {
        return vehicleRepository.saveAndFlush(vehicle);
    }

    @Override
    public void delete(Vehicle vehicle) {
        vehicleRepository.delete(vehicle);
    }

    @Override
    public Page<Vehicle> findVehiclesByType(VehicleType vehicleType, Pageable pageable) {
        return vehicleRepository.findVehiclesByStatusAndVehicleType(EntityStatus.ACTIVE, vehicleType, pageable);
    }

    @Override
    public Vehicle findVehicleByRegistrationNumber(String registrationNumber) {
        return vehicleRepository.findVehicleByRegistrationNumberEquals(registrationNumber);
    }

    @Override
    public Vehicle findVehicleById(Long id) {
        return vehicleRepository.findVehicleById(id);
    }

    @Override
    public List<Vehicle> findVehiclesByBookedFor(User user) {
        return vehicleRepository.findVehiclesByBookedFor(user);
    }

    @Override
    public Page<Vehicle> findVehiclesByStatusAndType(EntityStatus status, Collection<VehicleType> vehicleType, Pageable pageable) {
        return vehicleRepository.findVehiclesByStatusAndVehicleTypeIn(status, vehicleType, pageable);
    }
}
