package app.vehiclerenting.app.service;

import app.vehiclerenting.app.model.EntityStatus;
import app.vehiclerenting.app.model.User;
import app.vehiclerenting.app.model.Vehicle;
import app.vehiclerenting.app.model.VehicleType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface VehicleService extends BaseEntityService<Vehicle>{

    Page<Vehicle> findVehiclesByType(VehicleType vehicleType, Pageable pageable);

    Page<Vehicle> findVehiclesByStatusAndType(EntityStatus status, Collection<VehicleType> vehicleType, Pageable pageable);

    Vehicle findVehicleByRegistrationNumber(String registrationNumber);

    Vehicle findVehicleById(Long id);

    List<Vehicle> findVehiclesByBookedFor(User user);
}
