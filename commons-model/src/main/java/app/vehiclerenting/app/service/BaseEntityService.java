package app.vehiclerenting.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BaseEntityService<T> {
    T create(T t);

    Page<T> list(Pageable pageable);

    T edit(T t);

    void delete(T t);
}
