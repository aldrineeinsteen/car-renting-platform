package app.vehiclerenting.app.repository;

import app.vehiclerenting.app.model.EntityStatus;
import app.vehiclerenting.app.model.User;
import app.vehiclerenting.app.model.Vehicle;
import app.vehiclerenting.app.model.VehicleType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    List<Vehicle> findAllByVehicleTypeIn(Collection<VehicleType> vehicleType);

    Vehicle findVehicleByRegistrationNumberEquals(String registrationNumber);

    Page<Vehicle> findVehiclesByStatus(EntityStatus status, Pageable pageable);

    Page<Vehicle> findVehiclesByStatusAndVehicleType(EntityStatus status, VehicleType type, Pageable pageable);

    Page<Vehicle> findVehiclesByStatusAndVehicleTypeIn(EntityStatus status, Collection<VehicleType> types, Pageable pageable);

    Vehicle findVehicleById(Long id);

    List<Vehicle> findVehiclesByBookedFor(User bookedFor);
}
