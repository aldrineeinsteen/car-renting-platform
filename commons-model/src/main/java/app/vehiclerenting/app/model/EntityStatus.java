package app.vehiclerenting.app.model;

public enum EntityStatus {
    ACTIVE, UNDER_REVIEW, REMOVED, BOOKED
}
