package app.vehiclerenting.app.model;

public enum VehicleType {
    CAR, TRUCK
}
