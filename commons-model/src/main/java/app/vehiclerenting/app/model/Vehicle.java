package app.vehiclerenting.app.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
public class Vehicle extends BaseEntity {

    @NotEmpty
    private String brand;

    @NotEmpty
    private String name;

    @NotNull
    private Integer yearOfManufacture;

    private VehicleType vehicleType;

    @Transient
    private ZonedDateTime createdTime;

    @Transient
    private ZonedDateTime updatedTime;

    private String country;

    private int capacity;

    private String location;

    @Column(unique = true)
    private String registrationNumber;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    private User owner;

    private String imageLink;

    private EntityStatus status;

    @Transient
    @JsonInclude
    private String action;

    @Transient
    private boolean isDeleteEnabled;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "BOOKED_USER_ID")
    User bookedFor;

    @Transient
    @JsonInclude
    User releasedFor;
}
