package app.vehiclerenting.app.model;

public enum Action {
    APPROVE, REJECT, DELETE, BOOKED, RELEASE
}
