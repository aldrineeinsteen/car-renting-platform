package app.vehiclerenting.app.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.ZonedDateTime;
import java.util.Set;

@Entity
@Getter
@Setter
public class User extends BaseEntity{

    @NotBlank(message = "Username is mandatory")
    private String username;

    @NotBlank(message = "E-Mail ID is mandatory")
    private String emailId;

    @NotBlank(message = "Password is mandatory")
    @JsonIgnore
    private String password;

    @Transient
    @JsonIgnore
    private String passwordConfirm;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "username")
    @JsonIgnore
    private Set<Role> roles;

    @Transient
    @JsonInclude
    private ZonedDateTime lastLogin;

    @Transient
    @JsonInclude
    private boolean isLogin;

    @Transient
    @JsonInclude
    private boolean isRegistration;

    private String photo;
}
