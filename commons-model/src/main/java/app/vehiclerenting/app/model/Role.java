package app.vehiclerenting.app.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Setter
@Getter
public class Role extends BaseEntity{

    private String name;

    private boolean base;
}
