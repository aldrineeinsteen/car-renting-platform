package app.vehiclerenting.app.repository;

import app.vehiclerenting.app.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {

    List<Role> findAllByBaseIsTrue();

}
