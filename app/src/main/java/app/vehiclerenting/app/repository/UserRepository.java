package app.vehiclerenting.app.repository;

import app.vehiclerenting.app.model.User;
import app.vehiclerenting.app.model.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>{
    User findByUsername(String username);
}
