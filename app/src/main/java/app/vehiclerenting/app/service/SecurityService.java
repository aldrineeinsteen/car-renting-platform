package app.vehiclerenting.app.service;

public interface SecurityService {
    boolean isAuthenticated();
    void login(String username, String password);
}
