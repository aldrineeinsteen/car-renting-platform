package app.vehiclerenting.app.service.impl;

import app.vehiclerenting.app.model.User;
import app.vehiclerenting.app.repository.RoleRepository;
import app.vehiclerenting.app.repository.UserRepository;
import app.vehiclerenting.app.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private PasswordEncoder bCryptPasswordEncoder;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        if (user.getRoles() == null || user.getRoles().isEmpty())
            user.setRoles(new HashSet<>(roleRepository.findAllByBaseIsTrue()));
        userRepository.save(user);
    }
}
