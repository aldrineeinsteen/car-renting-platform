package app.vehiclerenting.app.service;

import app.vehiclerenting.app.model.User;

public interface UserService {
    User findByUsername(String username);

    void save(User user);
}
