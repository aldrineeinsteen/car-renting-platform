package app.vehiclerenting.app.controller;

import app.vehiclerenting.app.util.FileUploadUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.InputStream;

@Controller
public class FileController {

    @GetMapping(value = "/user-file")
    public @ResponseBody
    byte[] getPhoto(@RequestParam("id") String id) throws IOException {
        return FileUploadUtil.retrieveFile(id);
    }
}
