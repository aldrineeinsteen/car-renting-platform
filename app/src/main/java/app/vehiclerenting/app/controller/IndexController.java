package app.vehiclerenting.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    /**
     * Retrieves the home page for the application.
     * @param model model object for the page
     * @return the thymeleaf template
     */
    @RequestMapping({"/", "/index", "/login"})
    String getIndex(Model model) {
        return "index";
    }
}
