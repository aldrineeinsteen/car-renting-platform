package app.vehiclerenting.app.controller;

import app.vehiclerenting.app.model.User;
import app.vehiclerenting.app.service.SecurityService;
import app.vehiclerenting.app.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@AllArgsConstructor
public class UserController {

    private final KafkaTemplate<String, User> kafkaTemplate;

    private UserService userService;

    private SecurityService securityService;

    /**
     * Shows the registration page, if the user is not authenticated.
     * @param model model data for the view.
     * @return the view for registration.
     */
    @GetMapping("/registration")
    public String registration(Model model) {
        if (securityService.isAuthenticated()) {
            return "redirect:/";
        }
        model.addAttribute("userForm", new User());
        return "registration";
    }

    /**
     * Controller for processing user registration.
     * @param userForm input from user
     * @param bindingResult binding results
     * @return if validation is successful, registers the user. and redirect the user for the login.
     */
    @PostMapping("/registration")
    public String registration(@Validated @ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.save(userForm);
        //securityService.login(userForm.getUsername(), userForm.getPasswordConfirm());
        userForm.setRegistration(true);
        kafkaTemplate.send(
                "auth",
                userForm
        );
        return "redirect:/index";
    }

//    @GetMapping("/login")
//    public String login(Model model, String error, String logout) {
//        if (securityService.isAuthenticated()) {
//            return "redirect:/";
//        }
//        if (error != null)
//            model.addAttribute("error", "Your username and password is invalid.");
//        if (logout != null)
//            model.addAttribute("message", "You have been logged out successfully.");
//        return "login";
//    }

    @GetMapping("/logout")
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/index";
    }
}
