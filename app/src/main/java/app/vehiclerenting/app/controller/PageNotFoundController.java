package app.vehiclerenting.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageNotFoundController {

    @GetMapping("/404")
    String get404(){
        return "/error/404";
    }
}
