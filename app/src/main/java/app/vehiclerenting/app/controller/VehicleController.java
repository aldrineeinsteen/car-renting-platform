package app.vehiclerenting.app.controller;

import app.vehiclerenting.app.model.*;
import app.vehiclerenting.app.service.UserService;
import app.vehiclerenting.app.service.VehicleService;
import app.vehiclerenting.app.util.FileUploadUtil;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@AllArgsConstructor
public class VehicleController {

    private final KafkaTemplate<String, Object> kafkaTemplate;
    private VehicleService vehicleService;
    private UserService userService;

    private final Logger logger = LogManager.getLogger(VehicleController.class);

    @RequestMapping(value = "/vehicle", method = RequestMethod.GET)
    public String list(Model model,
                       @RequestParam("page") Optional<Integer> page,
                       @RequestParam("size") Optional<Integer> size,
                       @RequestParam("type") Optional<String> type) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);
        String inputType = type.orElse("ALL");
        List<VehicleType> vehicleType = getVehicleTypes(inputType);
        logger.info("Retrieving with available vehicle list page - {}, size - {}, type - {}", currentPage, pageSize, vehicleType);
        String principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        User currentUser = userService.findByUsername(principal);
//        System.out.println(vehicleType);
        Page<Vehicle> list = vehicleService.findVehiclesByStatusAndType(
                EntityStatus.ACTIVE,
                vehicleType,
                PageRequest.of(currentPage - 1, pageSize));
        list.stream().forEach(vehicle -> {
            if(principal.equals(vehicle.getOwner().getUsername()))
                vehicle.setDeleteEnabled(true);
        });
        model.addAttribute("list", list);
        if (list != null && !list.isEmpty()) {
            int totalPages = list.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                        .boxed()
                        .collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
                model.addAttribute("type", inputType);
                model.addAttribute("currentUser", principal);
            }
        }
        List<Vehicle> vehiclesByBookedFor = vehicleService.findVehiclesByBookedFor(currentUser);
        model.addAttribute("booked", vehiclesByBookedFor);
        return "vehicle/list";
    }


    @RequestMapping(value = "/vehicle/approvals", method = RequestMethod.GET)
    public String listReviewPending(Model model,
                                    @RequestParam("page") Optional<Integer> page,
                                    @RequestParam("size") Optional<Integer> size,
                                    @RequestParam("type") Optional<String> type) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);
        String inputType = type.orElse("ALL");
        List<VehicleType> vehicleType = getVehicleTypes(inputType);
        logger.info("Retrieving with available vehicle list page - {}, size - {}, type - {}", currentPage, pageSize, vehicleType);
        Page<Vehicle> list = vehicleService.findVehiclesByStatusAndType(
                EntityStatus.UNDER_REVIEW,
                vehicleType,
                PageRequest.of(currentPage - 1, pageSize));
        model.addAttribute("list", list);
        if (list != null && !list.isEmpty()) {
            int totalPages = list.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                        .boxed()
                        .collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
        }
        return "vehicle/review";
    }

    private List<VehicleType> getVehicleTypes(String inputType) {
        List<VehicleType> vehicleType;
        switch (inputType) {
            case "CAR":
                vehicleType = List.of(new VehicleType[]{VehicleType.CAR});
                break;
            case "TRUCK":
                vehicleType = List.of(new VehicleType[]{VehicleType.TRUCK});
                break;
            default:
                vehicleType = List.of(new VehicleType[]{VehicleType.CAR, VehicleType.TRUCK});
        }
        return vehicleType;
    }

    @PostMapping("/vehicle/add_vehicle")
    public String save(
            Vehicle vehicle,
            Model model,
            @RequestParam("photo_image") MultipartFile multipartFile) throws IOException {

        String linkPhoto = UUID.randomUUID().toString();

        String uploadDir = "target/user-photos/";

        FileUploadUtil.saveFile(linkPhoto, multipartFile);

        vehicle.setStatus(EntityStatus.UNDER_REVIEW);
        vehicle.setImageLink(linkPhoto);
        vehicle.setOwner(userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()));
        vehicle = vehicleService.create(vehicle);
        model.addAttribute("vehicle", vehicle);
        kafkaTemplate.send(
                "vehicle",
                vehicle
        );
        return "redirect:/vehicle/add_success";
    }

    @GetMapping("/vehicle/add")
    public String getAddPage(
            Model model,
            String message) {
        return "vehicle/add";
    }

    @GetMapping("/vehicle/add_success")
    public String getAddSuccess(
            Model model,
            String message) {
        if (model.getAttribute("vehicle") == null) {
            model.addAttribute("error", "No registered vehicle found.");
            return "redirect:/vehicle/add";
        }
        return "vehicle/success";
    }

    @PostMapping("/vehicle/approve")
    public String postAccept(
            Model model,
            @RequestParam("id") Long id
    ) {
        Vehicle vehicleById = vehicleService.findVehicleById(id);
        vehicleById.setAction(String.valueOf(Action.APPROVE));
        kafkaTemplate.send(
                "vehicle",
                vehicleById
        );
        return "redirect:/vehicle/approvals";
    }

    @PostMapping("/vehicle/reject")
    public String postReject(
            Model model,
            @RequestParam("id") Long id
    ) {
        Vehicle vehicleById = vehicleService.findVehicleById(id);
        vehicleById.setAction(String.valueOf(Action.REJECT));
        kafkaTemplate.send(
                "vehicle",
                vehicleById
        );
        return "redirect:/vehicle/approvals";
    }

    @PostMapping("/vehicle/delete")
    public String postDelete(
            Model model,
            @RequestParam("id") Long id
    ) {
        Vehicle vehicleById = vehicleService.findVehicleById(id);
        vehicleById.setAction(String.valueOf(Action.DELETE));
        kafkaTemplate.send(
                "vehicle",
                vehicleById
        );
        return "redirect:/vehicle";
    }

    @PostMapping("/vehicle/book")
    public String postBook(
            Model model,
            @RequestParam("id") Long id
    ) {
        User currentUser = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        Vehicle vehicleById = vehicleService.findVehicleById(id);
        vehicleById.setStatus(EntityStatus.BOOKED);
        vehicleById.setAction(String.valueOf(Action.BOOKED));
        vehicleById.setBookedFor(currentUser);
        vehicleService.edit(vehicleById);
        kafkaTemplate.send(
                "vehicle",
                vehicleById
        );
        return "redirect:/vehicle";
    }


    @PostMapping("/vehicle/release")
    public String postRelease(
            Model model,
            @RequestParam("id") Long id
    ) {
        User currentUser = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        Vehicle vehicleById = vehicleService.findVehicleById(id);
        vehicleById.setStatus(EntityStatus.ACTIVE);
        vehicleById.setAction(String.valueOf(Action.RELEASE));
        vehicleById.setBookedFor(null);
        vehicleById.setReleasedFor(currentUser);
        vehicleService.edit(vehicleById);
        kafkaTemplate.send(
                "vehicle",
                vehicleById
        );
        return "redirect:/vehicle";
    }
}
