package app.vehiclerenting.app.configuration;

import app.vehiclerenting.app.model.User;
import app.vehiclerenting.app.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class AuthenticationConfig implements AuthenticationProvider {

    private PasswordEncoder passwordEncoder;

    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (authentication != null) {
            String username = authentication.getName();
            String passwordPhrase = authentication.getCredentials().toString();

            User byUsername = userService.findByUsername(username);
            if (byUsername != null) {
                if (passwordEncoder.matches(passwordPhrase, byUsername.getPassword())) {
                    List<GrantedAuthority> grantedAuths = new ArrayList<>();
                    byUsername.getRoles().forEach(role -> {
//                        System.out.println("Adding role: " + role.getName());
                        grantedAuths.add(new SimpleGrantedAuthority(role.getName()));
                    });
                    return new UsernamePasswordAuthenticationToken(username, "", grantedAuths);
                }
            }
        }
        throw new UsernameNotFoundException("Incorrect credentials.");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
