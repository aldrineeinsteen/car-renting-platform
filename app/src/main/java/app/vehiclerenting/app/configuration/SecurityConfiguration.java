package app.vehiclerenting.app.configuration;

import app.vehiclerenting.app.model.User;
import app.vehiclerenting.app.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.time.ZonedDateTime;

@Configuration
@AllArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;

    private UserService userService;

    private final KafkaTemplate<String, User> kafkaTemplate;

    private AuthenticationConfig authenticationConfig;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/h2-console/").permitAll()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/registration**").permitAll()
                .antMatchers("/404").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/actuator/**").permitAll()
                .antMatchers("/index").permitAll()
                .antMatchers("/static/**").permitAll()
                .antMatchers("/**").authenticated()

                .and()
                .formLogin()
                .loginPage("/login")
                .failureUrl("/index?error")
                .usernameParameter("username").passwordParameter("password")
                .successForwardUrl("/")
                .successHandler((request, response, authentication) -> {
                    String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
                    User authenticatedUser = userService.findByUsername(username);
                    authenticatedUser.setLastLogin(ZonedDateTime.now());
                    authenticatedUser.setLogin(true);

                    kafkaTemplate.send(
                            "auth",
                            authenticatedUser
                    );
                    response.sendRedirect(request.getContextPath());
                })

                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/index?logout")

                .and()
                .exceptionHandling().accessDeniedPage("/403")

                .and()
                .headers().frameOptions().sameOrigin()

                .and()
                .csrf()
                .ignoringAntMatchers("/h2-console/**");
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        auth
                .authenticationProvider(authenticationConfig);
    }
}
