package app.vehiclerenting.app.configuration;

import app.vehiclerenting.app.model.*;
import app.vehiclerenting.app.repository.RoleRepository;
import app.vehiclerenting.app.repository.UserRepository;
import app.vehiclerenting.app.repository.VehicleRepository;
import app.vehiclerenting.app.util.FileUploadUtil;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.HashSet;
import java.util.UUID;

@Profile("first")
@Component
@AllArgsConstructor
public class DataSetUpCLR implements CommandLineRunner {

    private RoleRepository roleRepository;

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private VehicleRepository vehicleRepository;

    @Override
    public void run(String... args) throws IOException {
        Role user = new Role();
        user.setName("USER");
        user.setBase(true);

        roleRepository.saveAndFlush(user);

        Role admin = new Role();
        admin.setName("ADMIN");
        admin.setBase(false);

        roleRepository.saveAndFlush(admin);

        User adminUser = new User();
        adminUser.setUsername("admin");
        adminUser.setEmailId("admin@example.com");
        adminUser.setPassword(passwordEncoder.encode("Admin@123"));
        HashSet<Role> adminRoles = new HashSet<>(roleRepository.findAll());
        adminUser.setRoles(adminRoles);


        User regUser = new User();
        regUser.setUsername("user");
        regUser.setEmailId("user@example.com");
        regUser.setPassword(passwordEncoder.encode("User@123"));
        HashSet<Role> regRoles = new HashSet<>(roleRepository.findAllByBaseIsTrue());
        regUser.setRoles(regRoles);

        String car1link = UUID.randomUUID().toString();
        InputStream car1 = this.getClass().getResourceAsStream("/car1.jpeg");
        FileUploadUtil.saveFile(car1link, car1);

        String car2link = UUID.randomUUID().toString();
        InputStream car2 = this.getClass().getResourceAsStream("/car2.jpg");
        FileUploadUtil.saveFile(car2link, car2);

        String car3link = UUID.randomUUID().toString();
        InputStream car3 = this.getClass().getResourceAsStream("/car3.jpg");
        FileUploadUtil.saveFile(car3link, car3);

        String truck1link = UUID.randomUUID().toString();
        InputStream truck1 = this.getClass().getResourceAsStream("/truck1.jpg");
        FileUploadUtil.saveFile(truck1link, truck1);

        adminUser = userRepository.save(adminUser);
        regUser = userRepository.save(regUser);

        vehicleRepository.save(
                createCar(
                        "Audi",
                        "Car 1",
                        "UK",
                        4,
                        "London",
                        2022,
                        car1link,
                        regUser
                )
        );
        vehicleRepository.save(
                createCar(
                        "Ford",
                        "Car 2",
                        "NA",
                        6,
                        "New York",
                        2021,
                        car2link,
                        adminUser
                )
        );
        vehicleRepository.save(
                createCar(
                        "Audi",
                        "Car 3",
                        "IN",
                        5,
                        "Chennai",
                        2018,
                        car3link,
                        regUser
                )
        );
        vehicleRepository.save(
                createTruck(
                        "Ford",
                        "Monster",
                        "US",
                        2,
                        "Texas",
                        2015,
                        truck1link,
                        adminUser
                )
        );
    }

    private Vehicle createCar(String brand, String name, String country, int capacity, String location, int yearOfManufacture, String imageLink, User user) {
        Vehicle ve = new Vehicle();
        ve.setBrand(brand);
        ve.setName(name);
        ve.setCountry(country);
        ve.setCapacity(capacity);
        ve.setLocation(location);
        ve.setVehicleType(VehicleType.CAR);
        ve.setYearOfManufacture(yearOfManufacture);
        ve.setStatus(EntityStatus.ACTIVE);
        ve.setImageLink(imageLink);
        ve.setOwner(user);

        return ve;
    }

    private Vehicle createTruck(String brand, String name, String country, int capacity, String location, int yearOfManufacture, String imageLink, User user) {
        Vehicle ve = new Vehicle();
        ve.setBrand(brand);
        ve.setName(name);
        ve.setCountry(country);
        ve.setCapacity(capacity);
        ve.setLocation(location);
        ve.setVehicleType(VehicleType.TRUCK);
        ve.setYearOfManufacture(yearOfManufacture);
        ve.setStatus(EntityStatus.ACTIVE);
        ve.setImageLink(imageLink);
        ve.setOwner(user);

        return ve;
    }
}
