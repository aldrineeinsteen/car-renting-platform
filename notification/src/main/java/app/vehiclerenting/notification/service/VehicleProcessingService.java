package app.vehiclerenting.notification.service;

import app.vehiclerenting.app.model.Action;
import app.vehiclerenting.app.model.EntityStatus;
import app.vehiclerenting.app.model.User;
import app.vehiclerenting.app.model.Vehicle;
import app.vehiclerenting.app.repository.VehicleRepository;
import app.vehiclerenting.notification.util.NotiGateConstants;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class VehicleProcessingService {
    private VehicleRepository vehicleRepository;

    private static final Logger logger = LogManager.getLogger(KafkaConsumerService.class);

    private JavaMailSender mailSender;


    @KafkaListener(topics = "vehicle", groupId = "process_vehicle")
    public void processReview(Vehicle ve) {
        logger.info(
                "Processing Review : {} - {}",
                ve.getName(), ve.getAction()
        );
        if(ve.getAction() != null && ve.getAction().equals(String.valueOf(Action.APPROVE))) {
            ve.setStatus(EntityStatus.ACTIVE);
            vehicleRepository.save(ve);
            sendMail(ve.getOwner(), ve, "Congratulations on approval of addition to the platform", "The car (%s) - has been accepted into our database; congratulation; you are well on the way to generate passive income.");
        } else if(ve.getAction() != null && ve.getAction().equals(String.valueOf(Action.REJECT))) {
            vehicleRepository.delete(ve);
            sendMail(ve.getOwner(), ve, " - unfortunately the vehicle has been rejected", "The car (%s) - has been rejected and removed from out database; as it didn't meet the set standards.");
        }
    }

    private void sendMail(User ve, Vehicle ve1, String x, String format) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(NotiGateConstants.FROM_EMAIL);
        mailMessage.setTo(ve.getEmailId());
        mailMessage.setSubject(ve1.getName() + x);
        mailMessage.setText(String.format(format,
                ve1.getName()));
        mailSender.send(mailMessage);
    }

    @KafkaListener(topics = "vehicle", groupId = "process_deletion")
    public void processDeletion(Vehicle ve) {
        logger.info(
                "Processing Deletion : {} - {}",
                ve.getName(), ve.getAction()
        );
        if(ve.getAction() != null && ve.getAction().equals(String.valueOf(Action.DELETE))) {
            ve.setStatus(EntityStatus.REMOVED);
            vehicleRepository.saveAndFlush(ve);
            sendMail(ve.getOwner(), ve, " - has been removed successfully", "The car (%s) - has been removed successfully");
        }
    }


    @KafkaListener(topics = "vehicle", groupId = "process_booking")
    public void processBooking(Vehicle ve) {
        logger.info(
                "Processing Booking : {} - {}",
                ve.getName(), ve.getAction()
        );
        if(ve.getAction() != null && ve.getAction().equals(String.valueOf(Action.BOOKED))) {
            sendMail(ve.getOwner(), ve, " - is booked for today", "The car (%s) - is booked for today.");
            //Send Mail to Owner and BooKed.

            sendMail(ve.getBookedFor(), ve, " - has been reserved successfully", "The car (%s) - is reserved for you today");

        }
    }
}
