package app.vehiclerenting.notification.service;

import app.vehiclerenting.app.model.User;
import app.vehiclerenting.notification.util.NotiGateConstants;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class LoginNotificationService {

    private static final Logger logger = LogManager.getLogger(LoginNotificationService.class);

    private JavaMailSender mailSender;

    /**
     *
     * Filtering and routing has been used to send notification to users; when his/her account is accessed.
     *
     */
    @KafkaListener(topics = "auth", groupId = "mail_notification")
    public void consumeAuth(User authenticatedUser) {
        if (authenticatedUser.isLogin()) {
            SimpleMailMessage authMessage = new SimpleMailMessage();
            authMessage.setTo(authenticatedUser.getEmailId());
            authMessage.setFrom(NotiGateConstants.FROM_EMAIL);
            authMessage.setSubject(authenticatedUser.getUsername() + " - New Login to Vehicle Renting Platform");
            authMessage.setText("New login is detected for your account @ " + authenticatedUser.getLastLogin());
            mailSender.send(authMessage);
        }
    }
}
