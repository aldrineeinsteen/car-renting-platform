package app.vehiclerenting.notification.service;

public interface BasicAsyncService {
    void process();
}
