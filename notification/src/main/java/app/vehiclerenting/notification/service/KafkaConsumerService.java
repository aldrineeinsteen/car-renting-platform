package app.vehiclerenting.notification.service;

import app.vehiclerenting.app.model.Vehicle;
import app.vehiclerenting.app.repository.VehicleRepository;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class KafkaConsumerService implements BasicAsyncService {

    private VehicleRepository vehicleRepository;

    private static final Logger logger = LogManager.getLogger(KafkaConsumerService.class);

    private JavaMailSender mailSender;

    @KafkaListener(topics = "notification", groupId = "mail_notification")
    public void consumeMail(String message) {
        System.out.println((String.format("$$$$ => Consumed message: %s", message)));
    }

    @Override
    public void process() {

    }
}
