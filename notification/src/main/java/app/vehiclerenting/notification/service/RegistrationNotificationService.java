package app.vehiclerenting.notification.service;

import app.vehiclerenting.app.model.User;
import app.vehiclerenting.notification.util.NotiGateConstants;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegistrationNotificationService {

    private static final Logger logger = LogManager.getLogger(RegistrationNotificationService.class);

    private JavaMailSender mailSender;
    /**
     *
     * Filtering and routing has been used to send notification to newly registered users.
     *
     */
    @KafkaListener(topics = "auth", groupId = "registration_notification")
    public void consumeAuth(User authenticatedUser) {
        if (authenticatedUser.isRegistration()) {
            SimpleMailMessage authMessage = new SimpleMailMessage();
            authMessage.setTo(authenticatedUser.getEmailId());
            authMessage.setFrom(NotiGateConstants.FROM_EMAIL);
            authMessage.setSubject(authenticatedUser.getUsername() + " - Welcome to Vehicle Renting Platform");
            authMessage.setText("Welcome to Vehicle Renting Platform by Malik.");
            mailSender.send(authMessage);
        }
    }
}
