# Car Renting Platform

A fully functional car renting platform built using the principles of SOA.

## Mail Configuration
We use ethereal service for the SMTP bucketing for testing purpose.


| Param    | Value                        | Used  |
|----------|:-----------------------------|:-----:|
| Host     | smtp.ethereal.email          |  Yes  |
| Port     | 587                          |  $12  |
| Security | STARTTLS                     |  $1   |
| Username | nedra.doyle53@ethereal.email |  $1   |
| Password | dSgu8TH1GTSxFUZsjN           |  $1   |


## Runtime
### For message broker (Kafka)
```shell
./zookeeper-server-start.sh ../config/zookeeper.properties
./kafka-server-start.sh ../config/server.properties
```

### For Application
```shell
cd commons-model
./mvnw install
cd ..
cd notification
./mvnw package
java -jar ./target/notification.jar
```
```shell
cd commons-model
./mvnw install
cd ..
cd app
./mvnw package
java -jar ./target/app.jar
```

The above scripts will start two sets of service 
1. Application at 8080
1. Notification at 9090

The Notification is a service for managing stream and scheduling jobs on top of the consumed streams.
